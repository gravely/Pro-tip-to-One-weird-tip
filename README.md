pro-tip-to-one-weird-tip
=============

Chrome extension that replaces occurrences of 'Pro-tip' with 'One weird tip'

Forked from [Steve Frank's](https://github.com/panicsteve) excellent [Cloud-to-butt extension](https://github.com/panicsteve/cloud-to-butt)

[Direct download of crx file](https://github.com/grantstavely/Pro-tip-to-One-weird-tip/blob/master/ProTipToWeirdOldTip.crx?raw=true)


Installation
------------

In Chrome, choose Window > Extensions.  Drag ProTipToOneWeirdTip.crx into the page that appears.
