walk(document.body);

function walk(node)
{
	// I stole this function from here:
	// http://is.gd/mwZp7E

	var child, next;

	switch ( node.nodeType )
	{
		case 1:  // Element
		case 9:  // Document
		case 11: // Document fragment
			child = node.firstChild;
			while ( child )
			{
				next = child.nextSibling;
				walk(child);
				child = next;
			}
			break;

		case 3: // Text node
			handleText(node);
			break;
	}
}

function handleText(textNode)
{
	var v = textNode.nodeValue;
    v = v.replace(/\bPROTIP\b/g, "ONE WEIRD TIP");
    v = v.replace(/\bPRO-TIP\b/g, "ONE-WEIRD-TIP");
    v = v.replace(/\bPro Tip\b/g, "One Weird Tip");
    v = v.replace(/\bPro tip\b/g, "One weird tip");
    v = v.replace(/\bpro Tip\b/g, "one Weird Tip");
    v = v.replace(/\bpro tip\b/g, "one weird tip");
	v = v.replace(/\bPro-Tip\b/g, "One Weird Tip");
	v = v.replace(/\bPro-tip\b/g, "One weird tip");
	v = v.replace(/\bpro-Tip\b/g, "one Weird Tip");
	v = v.replace(/\bpro-tip\b/g, "one weird tip");
    v = v.replace(/\bProTip\b/g, "One Weird Tip");
    v = v.replace(/\bProtip\b/g, "One weird tip");
    v = v.replace(/\bprotip\b/g, "one weird tip");

	textNode.nodeValue = v;
}


